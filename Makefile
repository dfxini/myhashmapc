TARGET = hashmap_test

SRC = $(wildcard src/*.c)
HDRS = $(wildcard src/*.h)
OBJ = $(SRC:.c=.o)

CC = gcc
GDB = gdb
CFLAGS = -g

$(TARGET): $(OBJ)
	$(CC) -lm -o $@ $^

run:
	./$(TARGET)

.PHONY: clean
clean:
	rm -f $(OBJ) $(TARGET)
