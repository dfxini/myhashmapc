#include <stdio.h>
#include "mymap.h"

int main() {
    printf("Servus svet!\n");
    t_hm_hashmap* test_map = hm_create_hashmap();    
    hm_put(test_map, "kljuc123", "Data123");
    hm_put(test_map, "kljuc456", "Data456");
    hm_put(test_map, "kljuc789", "Data789");
    hm_put(test_map, "123kljuc", "123Data123");
    hm_put(test_map, "456kljuc123", "456Data456");
    printf("Data: %s\n", hm_get(test_map, "kljuc123"));
    hm_delete(test_map, "kljuc789");
    printf("Data: %s\n", hm_get(test_map, "kljuc123"));
    if (hm_get(test_map, "kljuc789") == NULL) {
        printf("Is NULL\n");
    } else {
        printf("Is not NULL\n");
    }
    hm_drop_map(test_map);
}
