#include "mymap.h"
#include "prime.h"
#include <string.h>
#include <stdlib.h>
#include <string.h>

#define INITAL_MAP_SIZE 53

static t_hm_entry DELETED_ENTRY = {NULL, NULL};

static size_t calc_hash(const char* key) {
    size_t hash = 0;
    int key_len = strlen(key);
    char c;
    while (c = *key++) {
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

static t_hm_entry* new_entry(const char* key, const char* val) {
    t_hm_entry* entry = malloc(sizeof(t_hm_entry));
    entry->key = strdup(key);
    entry->val = strdup(val);
    return entry;
}

static void del_entry(t_hm_entry* entry) {
    free(entry->key);
    free(entry->val);
    free(entry);
}

static int find_entry(t_hm_hashmap* map, const char* key) {
    int indx = calc_hash(key) % map->size;
    t_hm_entry* entry = map->items[indx];
    while (1) {
        if (entry == NULL || entry == &DELETED_ENTRY) return -1;
        if (strcmp(entry->key, key) == 0) return indx;
        indx = (indx + 1) % map->size;
    }
}

static t_hm_hashmap* new_sized_map(int base_size) {
    t_hm_hashmap* map = malloc(sizeof(t_hm_hashmap));
    map->count = 0;
    map->size = next_prime(base_size);
    map->items = calloc(map->size, sizeof(t_hm_hashmap));
    return map;
}

t_hm_hashmap* hm_create_hashmap() {
    t_hm_hashmap* map = new_sized_map(INITAL_MAP_SIZE);
    return map;
}

void hm_drop_map(t_hm_hashmap* map) {
    for (int i = 0; i < map->size; i++) {
        if (map->items[i] != NULL && map->items[i] != &DELETED_ENTRY) {
            del_entry(map->items[i]);
        }
    }
    free(map->items);
    free(map);
}

char* hm_get(t_hm_hashmap* map, const char* key) {
    int indx = find_entry(map, key);
    if (indx == -1) return NULL;
    return map->items[indx]->val;
}

static void resize_map(t_hm_hashmap* map, int size) {
    if (size < INITAL_MAP_SIZE) return;
    t_hm_hashmap* new_map = new_sized_map(size);
    for (int i = 0; i < map->size; i++) {
        t_hm_entry* entry = map->items[i];
        if (entry != NULL && entry != &DELETED_ENTRY) {
            hm_put(new_map, entry->key, entry->val);
        }
    }

    map->count = new_map->count;

    int tmp_size = map->size;
    map->size = new_map->size;
    new_map->size = map->size;

    t_hm_entry** tmp_items = map->items;
    map->items = new_map->items;
    new_map->items = map->items;

    hm_drop_map(new_map);
}

static void resize_up(t_hm_hashmap* map) {
    int new_size = map->size * 2;
    resize_map(map, new_size);
}

static void resize_down(t_hm_hashmap* map) {
    int new_size = map->size / 2;
    resize_map(map, new_size);
}

void hm_delete(t_hm_hashmap* map, const char* key) {
    if (map->count * 100 / map->size < 10) {
        resize_down(map);
    }
    int indx = find_entry(map, key);
    if (indx == -1) return;
    del_entry(map->items[indx]);
    map->items[indx] = &DELETED_ENTRY;
    map->count--;

    // fix table, we're using linear probing
    int repl_indx = indx;
    while (1) {
        indx = (indx + 1) % map->size;
        t_hm_entry* tmp_entry = map->items[indx];
        if (tmp_entry == NULL || tmp_entry == &DELETED_ENTRY) return; 
        int tmp_indx = calc_hash(tmp_entry->key) % map->size;
        
        // move pair backwards
        if (tmp_indx <= repl_indx) {
            map->items[repl_indx] = tmp_entry;
            map->items[indx] = &DELETED_ENTRY;
            repl_indx = indx;
        }
    }
}

void hm_put(t_hm_hashmap* map, const char* key, const char* val) {
    if (map->count * 100 / map->size > 70) {
        resize_up(map);
    }
    t_hm_entry* entry = new_entry(key, val);
    int indx = calc_hash(entry->key) % map->size;
    while (map->items[indx] != NULL) {
        if (entry->key == map->items[indx]->key) { // same key, delete old entry
            del_entry(map->items[indx]);
            break;
        }
        indx = (indx + 1) % map->size;
    } 
    map->items[indx] = entry;
    map->count++;
}