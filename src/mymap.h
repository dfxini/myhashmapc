#ifndef MYMAP_H
#define MYMAP_H

typedef struct hm_entry {
    char* key;
    char* val;
} t_hm_entry; 

typedef struct hm_hashmap {
    int count;
    int size;
    t_hm_entry** items;
} t_hm_hashmap;

void hm_put(t_hm_hashmap* map, const char* key, const char* val);
void hm_delete(t_hm_hashmap* map, const char* key);
void hm_drop_map(t_hm_hashmap* map);
t_hm_hashmap* hm_create_hashmap();
char* hm_get(t_hm_hashmap* map, const char* key);

#endif
