#ifndef PRIME_H
#define PRIME_H

int is_prime(const int n);
int next_prime(int n);

#endif